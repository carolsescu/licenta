import MySQLdb
import time
import sys
import csv
from igraph import *
import datetime

file = open('edges.ncol',"r+")
file.truncate()
# Open database connection
db = MySQLdb.connect("localhost","root","","licenta" )

cursor_list = db.cursor()
cursor = db.cursor()

cursor_arr = db.cursor()
cursor_dep = db.cursor()

departure_location = sys.argv[1]#raw_input("Enter departure lcoation: ")
arrival_location = sys.argv[2]#raw_input("Enter arrival lcoation: ")
departure_time = sys.argv[3]#raw_input("enter departure datetime (form YYYY-MM-DD HH:mm:SS): ")
arrival_time = sys.argv[4]#raw_input("enter departure datetime (form YYYY-MM-DD HH:mm:SS): ")
time_output = sys.argv[5]


start_time = time.clock()
cursor_arr.execute("SELECT DISTINCT arrival_terminal, arrival_date FROM trips WHERE departure_date > %s AND arrival_date < %s ORDER BY arrival_terminal, arrival_date", (departure_time, arrival_time))
cursor_dep.execute("SELECT DISTINCT departure_terminal, departure_date FROM trips WHERE arrival_date < %s AND departure_date > %s ORDER BY departure_terminal, departure_date", (arrival_time, departure_time))

if time_output == "Yes":
    print("--- %s seconds --- to run the queries for the layover edges" % (time.clock() - start_time))

current_arr = cursor_arr.fetchone()
current_dep = cursor_dep.fetchone()
timepoints = []

weight = "0"

while current_dep is not None or current_arr is not None:
    if current_dep is None:
        timepoints.append(current_arr)
        current_arr = cursor_arr.fetchone()
    elif current_arr is None:
        timepoints.append(current_dep)
        current_dep = cursor_dep.fetchone()
    elif current_arr[0] < current_dep[0]:
        timepoints.append(current_arr)
        current_arr = cursor_arr.fetchone()
    elif current_arr[0] > current_dep[0]:
        timepoints.append(current_dep)
        current_dep = cursor_dep.fetchone()
    else:
        if current_arr[1] < current_dep[1]:
            timepoints.append(current_arr)
            current_arr = cursor_arr.fetchone()
        elif current_arr[1] > current_dep[1]:
            timepoints.append(current_dep)
            current_dep = cursor_dep.fetchone()
        else:
            timepoints.append(current_arr)
            current_arr = cursor_arr.fetchone()
            current_dep = cursor_dep.fetchone()



previous_element = timepoints[0]
current_element = timepoints[1]
counter = 1

cursor.execute("SELECT meal, accommodation FROM layover WHERE city = %s", previous_element[0])
prices = cursor.fetchone()
weight_minute = (3 * prices[0] + prices[1])/1440

if previous_element[0] == departure_location:
    departure_toggle = True
    destination = (previous_element[0] + str(previous_element[1])).replace(" ", "")#change to previous for all
    source = (departure_location + str(departure_time)).replace(" ", "")
    row = source + " " + destination + " " + "0" + "\n"
    file.write(row)
else:
    departure_toggle = False
if previous_element[0] == arrival_location:
    arrival_toggle = True
    destination = (previous_element[0] + str(previous_element[1])).replace(" ", "")
    source = (arrival_location + str(arrival_time)).replace(" ", "")
    row = destination + " " + source + " " + "0" + "\n"
    file.write(row)
else:
    arrival_toggle = False

while counter < len(timepoints):
    previous_element = timepoints[counter-1]
    current_element = timepoints[counter]
    if current_element[0] == previous_element[0]:
        source = (previous_element[0] + str(previous_element[1])).replace(" ", "")
        destination = (current_element[0] + str(current_element[1])).replace(" ", "")
        weight = weight_minute * ((current_element[1] - previous_element[1]).total_seconds() / 60)
        row = source + " " + destination + " " + str(weight) + "\n"
        file.write(row)
        if departure_toggle:
            source = (departure_location + str(departure_time)).replace(" ", "")
            row = source + " " + destination + " " + "0" + "\n"
            file.write(row)
        if arrival_toggle:
            source = (arrival_location + str(arrival_time)).replace(" ","")
            row = destination + " " + source + " " + "0" + "\n"
            file.write(row)
    else:
        cursor.execute("SELECT meal, accommodation FROM layover WHERE city = %s", current_element[0])
        prices = cursor.fetchone()
        weight_minute = (3 * prices[0] + prices[1]) / 1440
        if current_element[0] == departure_location:
            departure_toggle = True
            destination = (current_element[0] + str(current_element[1])).replace(" ", "")
            source = (departure_location + str(departure_time)).replace(" ", "")
            row = source + " " + destination + " " + "0" + "\n"
            file.write(row)
        else:
            departure_toggle = False
        if current_element[0] == arrival_location:
            arrival_toggle = True
            destination = (current_element[0] + str(current_element[1])).replace(" ", "")
            source = (arrival_location + str(arrival_time)).replace(" ","")
            row = destination + " " + source + " " + "0" + "\n"
            file.write(row)
        else:
            arrival_toggle = False

    counter = counter + 1

if time_output == "yes":
    print("--- %s seconds --- to create layover edges" % (time.clock() - start_time))

cursor_list.execute("SELECT departure_terminal, departure_date, arrival_terminal, arrival_date, price FROM trips WHERE departure_date > %s AND arrival_date < %s", (departure_time, arrival_time))

if time_output == "yes":
    print("--- %s seconds --- to run the query for the trip edges" % (time.clock() - start_time))

current_element = cursor_list.fetchone()

while current_element is not None:
    source = (current_element[0] + str(current_element[1])).replace(" ", "")
    destination = (current_element[2] + str(current_element[3])).replace(" ", "")
    weight = str(current_element[4])
    row = source + " " + destination + " " + weight + "\n"
    file.write(row)
    current_element = cursor_list.fetchone()

file.close()

if time_output == "yes":
    print("--- %s seconds --- to generate the trip edges" % (time.clock() - start_time))# result printed in csv

previous = departure_location
graph = read('edges.ncol', format = "ncol", names = True, directed = True)

if time_output == "yes":
    print("--- %s seconds --- to read the graph data" % (time.clock() - start_time))

path = graph.get_shortest_paths((departure_location + departure_time).replace(" ",""), (arrival_location + arrival_time).replace(" ",""), "weight")
for n in path[0]:
    if graph.vs[n]['name'][:3] != previous[:3]:
        print previous + " ---- " + "{}".format(graph.vs[n]['name'])

    previous = graph.vs[n]['name']

if time_output == "yes":
    print("--- %s seconds --- to generate the path" % (time.clock() - start_time))

# disconnect from server 328817
db.close()

