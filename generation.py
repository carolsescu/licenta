import random
import math
import datetime
import sys
import MySQLdb
db = MySQLdb.connect("localhost", "root", "", "licenta" )
cursor = db.cursor()
cursor.execute("DROP TABLE IF EXISTS trips")
db.commit()
cursor.execute("CREATE TABLE trips(id INTEGER PRIMARY KEY AUTO_INCREMENT, departure_terminal VARCHAR(128), arrival_terminal VARCHAR(128), departure_date DATETIME, arrival_date DATETIME, price INT, INDEX `time` (`departure_date`, `arrival_date`))")
db.commit()

cursor.execute("DROP TABLE IF EXISTS layover")
db.commit()
cursor.execute("CREATE TABLE layover(id INTEGER PRIMARY KEY AUTO_INCREMENT, meal FLOAT, accommodation INT, city VARCHAR(128), INDEX cityname(city))")
db.commit()
factor = int(sys.argv[1])
nod = int(sys.argv[2])
j = 44
cities = []
for i in range(ord("A"),ord("A") + factor):
    for j in range(ord("A"),ord("Z") + 1):
        for k in range(ord("A"),ord("Z") + 1):
            cities.append(chr(i) + chr(j) + chr(k))
totalCount = 0
defaultDate = datetime.datetime(2018, 3, 6)
for i in range(len(cities)):
    count = 0
    mealPriceRand = random.random()
    mealPrice = 4 + 2 * mealPriceRand  # meal prices set between 4 and 6
    accommodPriceRand = random.random()
    accommodPrice = 10 + math.floor(accommodPriceRand * 31)
    cursor.execute("INSERT INTO layover(city, meal, accommodation) VALUES(%s, %s, %s)", (cities[i], mealPrice, accommodPrice))
    for j in range(len(cities)):
        if i!=j:
            trial = random.random()
            durationRand = random.random()
            departureRand = math.floor(random.random() * 24 * 60)#randomizing the minutes only as we will make each trip a daily one (assumption)
            departureRandReturn = math.floor(random.random() * 24 * 60)
            diff = abs(i-j)
            if diff <= 5:
                if trial < 0.9:
                    duration = 120 + math.floor(durationRand * 180) #short distances will be between 2-5 hours
                    for k in range(nod):
                        priceRand = random.random()
                        priceRandReturn = random.random()

                        price = 5 * math.ceil(priceRand*5)#generally prices are in larger increments
                        priceReturn = 5 * math.ceil(priceRandReturn * 5)

                        startDateTime = defaultDate + datetime.timedelta(minutes = departureRand) + datetime.timedelta(days = k)
                        startDateTimeReturn = defaultDate + datetime.timedelta(minutes=departureRandReturn) + datetime.timedelta(days=k)

                        endDateTime = startDateTime + datetime.timedelta(minutes=duration)
                        endDateTimeReturn = startDateTimeReturn + datetime.timedelta(minutes=duration)

                        cursor.execute('INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)', (cities[i], cities[j], startDateTime, endDateTime, price))
                        cursor.execute('INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)', (cities[j], cities[i], startDateTimeReturn, endDateTimeReturn, priceReturn))
            elif diff <= 10:
                if trial < 0.7:
                    duration = 300 + math.floor(durationRand * 300) #longer distances will be between 5-10 hours
                    for k in range(nod):
                        priceRand = random.random()
                        priceRandReturn = random.random()

                        price = 10 * (math.ceil(priceRand * 5))
                        priceReturn = 10 * math.ceil(priceRandReturn * 5)

                        startDateTime = defaultDate + datetime.timedelta(minutes = departureRand) + datetime.timedelta(days = k)
                        startDateTimeReturn = defaultDate + datetime.timedelta(minutes=departureRandReturn) + datetime.timedelta(days=k)

                        endDateTime = startDateTime + datetime.timedelta(minutes = duration)
                        endDateTimeReturn = startDateTimeReturn + datetime.timedelta(minutes=duration)

                        cursor.execute('''INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)''', (cities[i],cities[j], startDateTime, endDateTime, price))
                        cursor.execute('INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)', (cities[j], cities[i], startDateTimeReturn, endDateTimeReturn, priceReturn))
            else:
                if trial < 20.0 / len(cities):#each city connected to approximately 20 other cities by large distances
                    duration = 60 + math.floor(durationRand * 120) #large distances will be lower to mimic flights 1-3 hours
                    for k in range(nod):
                        priceRand = random.random()
                        priceRandReturn = random.random()

                        price = 20 * math.ceil(priceRand * 10)
                        priceReturn = 20 * math.ceil(priceRandReturn * 10)

                        startDateTime = defaultDate + datetime.timedelta(minutes = departureRand) + datetime.timedelta(days = k)
                        startDateTimeReturn = defaultDate + datetime.timedelta(minutes=departureRandReturn) + datetime.timedelta(days=k)

                        endDateTime = startDateTime + datetime.timedelta(minutes = duration)
                        endDateTimeReturn = startDateTimeReturn + datetime.timedelta(minutes=duration)

                        cursor.execute('''INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)''', (cities[i],cities[j], startDateTime, endDateTime, price))
                        cursor.execute('INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)', (cities[j], cities[i], startDateTimeReturn, endDateTimeReturn, priceReturn))
    db.commit()
db.commit()
cursor.execute('''SELECT * FROM trips LIMIT 10''')
all_rows = cursor.fetchall()
for row in all_rows:
    print('{0} - {1} from {2} to {3} at price {4}'.format(row[1], row[2], row[3], row[4], row[5]))
db.close()
# it works
