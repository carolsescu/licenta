import time
import random
import math
import datetime
import MySQLdb
import sys

starttime = time.time()

db = MySQLdb.connect("localhost","root","","licenta" )
seconds = float(sys.argv[1])
cursor = db.cursor()

while True:
    cursor.execute("SELECT COUNT(*) FROM `trips` WHERE 1")
    flights = cursor.fetchone()[0]
    random_flight_id = int(random.random() * flights)
    cursor.execute("SELECT departure_terminal, arrival_terminal FROM `trips` WHERE id = " + str(random_flight_id))

    random_flight = cursor.fetchone()

    distance = abs((ord(random_flight[0][0]) * 676 + ord(random_flight[0][1]) * 26 + ord(random_flight[0][2])) - (ord(random_flight[1][0]) * 676 + ord(random_flight[1][1]) * 26 + ord(random_flight[1][2])))

    if distance <= 5:
        priceRand = random.random()
        price = 5 * math.ceil(priceRand * 5)
        cursor.execute('''UPDATE trips SET price = %s WHERE id = %s''', (price, random_flight_id))
    elif distance <= 10:
        priceRand = random.random()
        price = 10 * (math.ceil(priceRand * 5))
        cursor.execute('''UPDATE trips SET price = %s WHERE id = %s''', (price, random_flight_id))
    else:
        priceRand = random.random()
        price = 20 * math.ceil(priceRand * 10)
        cursor.execute('''UPDATE trips SET price = %s WHERE id = %s''', (price, random_flight_id))
    db.commit()


    print random_flight_id
    print random_flight
    print distance
    print price
    print "UPDATE trips SET price = " + str(price) + " WHERE id = " + str(random_flight_id)
    time.sleep(seconds - ((time.time() - starttime) % seconds))