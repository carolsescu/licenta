import csv
import time
#import sqlite3
import MySQLdb
# db = sqlite3.connect('trips')
db = MySQLdb.connect("localhost","root","","licenta" )
start_time = time.clock()
# f = open('trips.csv')
graph_readable = open('edges.ncol', 'wb') # maybe transform this into database entries
# csv_f = csv.reader(f)
cursor = db.cursor()
cursor_arr = db.cursor()
cursor_dep = db.cursor()
cursor_tot = db.cursor()
cursor_arr.execute("SELECT arrival_terminal, arrival_date FROM trips ORDER BY arrival_terminal, arrival_date ASC")
cursor_dep.execute("SELECT departure_terminal, departure_date FROM trips ORDER BY departure_terminal, departure_date ASC")
current_arrival = cursor_arr.fetchone()
current_departure = cursor_dep.fetchone()
previous_departure = current_departure
cursor_tot.execute("SELECT COUNT(*) FROM trips")
total = cursor_tot.fetchone()[0]
departure_counter = arrival_counter = price = 0
array = []
while departure_counter + arrival_counter < total * 2:
    if current_arrival[1] < current_departure[1] and current_departure[0] == current_arrival[0] and departure_counter <= total and arrival_counter <= total:
        array.append(current_arrival)
        current_arrival = cursor_arr.fetchone()
        arrival_counter += 1
    else:
        for element in array:
            #print element[0] + current_departure[0] + element[1] + current_departure[1]
            if element[0] != current_departure[0]:
                print "WRONG!"
                #print element[0] + current_departure[0] + element[1] + current_departure[1]
                break
            cursor.execute('''INSERT INTO trips(departure_terminal, arrival_terminal, departure_date, arrival_date, price) VALUES(%s,%s,%s,%s,%s)''',
                           (element[0], current_departure[0], element[1], current_departure[1], price))
        if (current_departure[0] != previous_departure[0] and current_departure[0] != current_arrival[0]) or departure_counter == total:
            array = []
            arrival_counter += 1
            try:
                current_arrival = cursor_arr.fetchone()
            except StopIteration:
                break
        else:
            departure_counter += 1
            previous_departure = current_departure
            current_departure = cursor_dep.fetchone()
cursor.execute("SELECT * FROM trips")
for row in cursor:
    new_row = ""
    new_row += (row[1] + row[3]).replace(" ", "") + " "
    new_row += (row[2] + row[4]).replace(" ", "") + " "
    new_row += str(row[5]) + "\n"
    graph_readable.write(new_row)
print("--- %s seconds ---" % (time.clock() - start_time))